# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location

curl -o sip-4.19.25.zip https://www.riverbankcomputing.com/static/Downloads/sip/4.19.25/sip-4.19.25.zip
7z x sip-4.19.25.zip
cd sip-4.19.25
python configure.py -b "$toppath\python" -d "$toppath\python\Lib\site-packages" -e "$toppath\python\include" -v "$toppath\python\sip"
nmake
nmake install

cd "$toppath\python"
7z a -r ..\sip.zip *
